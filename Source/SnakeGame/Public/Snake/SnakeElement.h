#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/Interactable.h"
#include "SnakeElement.generated.h"

UCLASS()
class SNAKEGAME_API ASnakeElement : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElement();

private:
	//** Root component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* BoxCollision;

	/** Snake head static mesh */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Snake", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* SnakeElement_SM;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void Interact(AActor* Interacted) override;

	/** Animation snake element death */
	void SnakeElementDead(FVector IpulseVector, float MassElement);

};
