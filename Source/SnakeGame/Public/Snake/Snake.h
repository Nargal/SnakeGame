#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snake.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSnakeDeath);

UCLASS()
class SNAKEGAME_API ASnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnake();

	// Delegate snake death
	FOnSnakeDeath OnSnakeDeath;

private:
	//** Root component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* BoxCollision;

	/** Snake head static mesh */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Snake", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* Head_SM;

	/** Snake element class */
	UPROPERTY(EditDefaultsOnly, Category = "Snake")
	TSubclassOf<class ASnakeElement> SnakeElementClass;

	/** Array to store element of the snake */
	TArray<class ASnakeElement*> SnakeElements;

	/** The sound of the snake's death */
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	class USoundBase* SnakeDestroySound;

	/** Save Tick delta time */
	float BufferTime;
	
	/** Interval for the snake step */
	float StepDelay;

	// Size step grid
	float StepSize;

	// Size of a single element
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Params")
	float ElementSize;

	// Scale of a single element
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Params")
	float ElementScale;

	// The padding of the element
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Params")
	float ElementPadding;
	
	/** Direction move the snake head */
	FVector MoveDirection;

	/** Current direction of movement of the snake's head */
	FVector CurrentMoveDir;

	/** Previous location of the snake's head */
	FVector PrevElementLocation;

	class IInteractable* Interface;

	UPROPERTY()
	class UParticleSystemComponent* PS_HeadDead;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX", meta = (AllowPrivateAccess = "true"))
	class UParticleSystem* HeadDead_FX;

	bool bIsAlive;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddNewSnakeElement(int32 NumElement = 1);

	// Set new snake movement direction
	void SetMovementDirection(FVector NewDirection);

	// Return current direction of movement of the snake's head
	FVector GetCurrentMoveDir();

	UFUNCTION()
	void SnakeElementOverlap(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void StartSnakeDead();

	void SnakeDeath();

	/** To change the delay before the movement of the snake */
	void ChangeDelayMoving(float Value);

	/** To change the delay before the movement of the snake */
	void DestroySnake();

private:
	/** The snake moves once in the time interval */
	void MoveSnake();
};
