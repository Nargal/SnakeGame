#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SnakeMainUI.generated.h"

UCLASS()
class SNAKEGAME_API USnakeMainUI : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent)
	void GameOver();
	void GameOver_Implementation();
	
};