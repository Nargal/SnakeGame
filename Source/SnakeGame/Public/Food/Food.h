#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/Interactable.h"
#include "Food.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSpawnFood);

UENUM()
enum class EFoodType
{
	FirstType,
	SecondType,
	ThirdTipe,
	FourthType
};

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	// Delegate spawn new food
	FOnSpawnFood OnSpawnFood;

private:
	//** Root component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* BoxCollision;

	/** Snake food static mesh */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Snake", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* Food_SM;

	/** Visual display of score */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Materials", meta = (AllowPrivateAccess = "true"))
	class UMaterialInstance* MatFirstType;
	/** Visual display of score */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Materials", meta = (AllowPrivateAccess = "true"))
	class UMaterialInstance* MatSecondType;
	/** Visual display of score */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Materials", meta = (AllowPrivateAccess = "true"))
	class UMaterialInstance* MatThirdType;
	/** Visual display of score */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Materials", meta = (AllowPrivateAccess = "true"))
	class UMaterialInstance* MatFourthType;

	/** The sound eating food */
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	class USoundBase* EatingFoodSound;

	/** Snake game mode actor */
	class ASnakeGameMode* GM;

	/** Game points */
	int32 Score;

	/** Change score timer */
	FTimerHandle UpdateScoreTimer;

	/** Interval for changing points */
	float UpdateSecond;

	/** Current food type */
	EFoodType LastFoodType;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Interaction of food with snakes
	virtual void Interact(AActor* Interacted) override;

private:
	// Start function UpdateScore through interval
	void ChangeFoodType();
	// Set the current number of points
	void UpdateScore();

};