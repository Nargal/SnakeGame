#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridGenerator.generated.h"

UCLASS()
class SNAKEGAME_API AGridGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridGenerator();

private:
	/** Root component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneRootComponent;

	UPROPERTY(VisibleAnywhere, Category = "Grid")
	class UHierarchicalInstancedStaticMeshComponent* GridElement_HISM;

	/** Class used to create a collision */
	UPROPERTY(EditDefaultsOnly, Category = "Grid")
	TSubclassOf<class AGridElementCollision> ElementCollisionClass;

	/** Array to store an element collision */
	TArray<class AGridElementCollision*> ElementsCollision;

	/** Array to store food spawn points */
	TArray<FVector> SpawnElemsLocation;

public:
	/**
	 * Grid Generation creates a grid of elements
	 * 
	 * @param	ElementsX			Number of elements on the X-axis
	 * @param	ElementsY			Number of elements on the Y-axis
	 * @param	ElementSize			Size of a single element
	 * @param	ElementScale		Scale of a single element
	 * @param	ElementPadding		The padding of the element
	 * 
	 */
	void GridGeneration(int32 ElementsX, int32 ElementsY, float ElementSize, float ElementScale, float ElementPadding);

	/**
	 * Create Collision creates a collision grid
	 *
	 * @param	ElementSize			Size of a single element
	 * @param	ElementScale		Scale of a single element
	 * @param	SpawnLocation		Point spawn grid element
	 *
	 */
	void CreateCollision(float ElementSize, float ElementScale, FVector SpawnLocation);

	/** Get a random spawn point from an array */
	FVector GetRandomLocation();

	/** Destroy all grid element */
	void DestroyGrid();

};