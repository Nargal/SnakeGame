#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/Interactable.h"
#include "GridElementCollision.generated.h"

UCLASS()
class SNAKEGAME_API AGridElementCollision : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridElementCollision();

private:
	/** Root component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneRootComponent;

	/** Box collision */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* BoxCollision;

public:
	/** The function sets the size box */
	void SetBoxSize(float Size, float Scale);

	virtual void Interact(AActor* Interacted) override;
};