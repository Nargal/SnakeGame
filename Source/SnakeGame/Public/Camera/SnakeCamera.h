#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SnakeCamera.generated.h"

UCLASS()
class SNAKEGAME_API ASnakeCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASnakeCamera();

private:
	/** Root component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneRootComponent;

	/** Camera boom positioning the camera above the pawn */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Snake camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SnakeCameraComponent;

	/** Keep a link to the snake */
	class ASnake* SnakeRef;

public:	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Get a link to the snake
	void SetSnakeActor(class ASnake* Snake);

private:
	/** Upward movement of the snake */
	void MovementUp();
	/** Downward movement of the snake */
	void MovementDown();
	/** Snake movement to the left */
	void MovementLeft();
	/** Snake movement to the right */
	void MovementRight();

	/** Restart after the game ends */
	void RestartGame();

};