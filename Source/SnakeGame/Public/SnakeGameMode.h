#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameMode.generated.h"

UCLASS()
class SNAKEGAME_API ASnakeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASnakeGameMode();

private:
	/** Default controller */
	class APlayerController* DefaultPC;

	/** Camera and input */
	class ASnakeCamera* SnakePawn;

	/** UI actor ref */
	class USnakeMainUI* SnakeUIRef;

	/** UI class */
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Class")
	TSubclassOf<class USnakeMainUI> SnakeClassUI;
	
	/** Grid generator ref */
	class AGridGenerator* GridActorRef;

	/** Grid class generator */
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Class")
	TSubclassOf<class AGridGenerator> GridActorClass;

	/** Snake actor reference */
	class ASnake* SnakeRef;

	/** Snake class */
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Class")
	TSubclassOf<class ASnake> SnakeClass;

	class AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly, Category = "Snake|Class")
	TSubclassOf<class AFood> FoodClass;

	// Snake movement step delay
	UPROPERTY(EditDefaultsOnly, Category = "Snake")
	float StepDelay;
	
	/** Snake params */
	// Number of elements on the X-axis
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Params")
	int32 GridSizeX;

	// Number of elements on the Y-axis
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Params")
	int32 GridSizeY;

	// Size of a single element
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Params")
	float ElementSize;

	// Scale of a single element
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Params")
	float ElementScale;

	// The padding of the element
	UPROPERTY(EditDefaultsOnly, Category = "Snake|Params")
	float ElementPadding;

	// The padding of the element
	int32 Score;

	// Game status
	bool bGame;

protected:
	virtual void BeginPlay() override;

private:
	/** Start new game */
	void StartGame();

	// Create main HUD
	void CreateSnakeUI();

	// Creating a game map
	void GeneratingMap();

	// Create and spawn new snake
	void CreateSnake();	
	
	// Create and spawn new food
	UFUNCTION()
	void SpawnNewFood();

	UFUNCTION()
	void SetGameOverUI();

public:
	/** Restarting the game */
	void RestartingGame();	

	/** Increase score by Value */
	void AddScore(int32 Value);
	
	/** Return current score */
	UFUNCTION(BlueprintCallable)
	int32 GetScore();

	// Return Snake movement step delay
	float GetStepDelay();
	// Returns the size of a single element
	float GetElementSize();
	// Return element scale
	float GetElementScale();
	// Return element padding
	float GetElementPadding();
	// Return game status
	bool GetGameStatus();
};