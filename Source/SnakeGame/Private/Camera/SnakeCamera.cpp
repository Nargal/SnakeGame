#include "Camera/SnakeCamera.h"

// UE4 class include
#include "Components/SceneComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// My class include
#include "SnakeGameMode.h"
#include "Snake/Snake.h"

// Sets default values
ASnakeCamera::ASnakeCamera()
{
	// Rood component
	SceneRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRootComponent"));
	RootComponent = SceneRootComponent;

	// Camera boom positioning the camera above the pawn
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->TargetArmLength = 1800.f;
	CameraBoom->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->SetupAttachment(RootComponent);

	// Snake camera
	SnakeCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SnakeCamera"));
	SnakeCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
}

// Called to bind functionality to input
void ASnakeCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	/** Bind snake movement event */
	PlayerInputComponent->BindAction("MovementUp", IE_Pressed, this, &ASnakeCamera::MovementUp);
	PlayerInputComponent->BindAction("MovementDown", IE_Pressed, this, &ASnakeCamera::MovementDown);
	PlayerInputComponent->BindAction("MovementLeft", IE_Pressed, this, &ASnakeCamera::MovementLeft);
	PlayerInputComponent->BindAction("MovementRight", IE_Pressed, this, &ASnakeCamera::MovementRight);

	PlayerInputComponent->BindAction("RestartGame", IE_Pressed, this, &ASnakeCamera::RestartGame);
}

// Get a link to the snake
void ASnakeCamera::SetSnakeActor(class ASnake* Snake)
{
	SnakeRef = Snake;
}

// Upward movement of the snake
void ASnakeCamera::MovementUp()
{
	if (SnakeRef != nullptr)
	{
		if (SnakeRef->GetCurrentMoveDir().X == 0)
		{
			SnakeRef->SetMovementDirection(FVector(1.f, 0.f, 0.f));
		}
	}	
}

// Downward movement of the snake
void ASnakeCamera::MovementDown()
{
	if (SnakeRef != nullptr)
	{
		if (SnakeRef->GetCurrentMoveDir().X == 0)
		{
			SnakeRef->SetMovementDirection(FVector(-1.f, 0.f, 0.f));
		}
	}
}

// Snake movement to the left
void ASnakeCamera::MovementLeft()
{
	if (SnakeRef != nullptr)
	{
		if (SnakeRef->GetCurrentMoveDir().Y == 0)
		{
			SnakeRef->SetMovementDirection(FVector(0.f, -1.f, 0.f));;
		}
	}	
}

// Snake movement to the right
void ASnakeCamera::MovementRight()
{
	if (SnakeRef != nullptr)
	{
		if (SnakeRef->GetCurrentMoveDir().Y == 0)
		{
			SnakeRef->SetMovementDirection(FVector(0.f, 1.f, 0.f));
		}
	}
}

/** Restart after the game ends */
void ASnakeCamera::RestartGame()
{
	auto GM = Cast<ASnakeGameMode>(GetWorld()->GetAuthGameMode());
	if (IsValid(GM) && !GM->GetGameStatus()) GM->RestartingGame();
}