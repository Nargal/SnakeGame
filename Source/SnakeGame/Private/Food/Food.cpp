#include "Food/Food.h"

// UE4 class include
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"

//My class include
#include "SnakeGameMode.h"
#include "Snake/Snake.h"

// Sets default values
AFood::AFood()
{
	// Root component
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetBoxExtent(FVector(50.f));
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxCollision->SetCollisionObjectType(ECC_PhysicsBody);
	BoxCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxCollision->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);
	RootComponent = BoxCollision;

	// Snake food static mesh
	Food_SM = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Food_SM"));
	Food_SM->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Food_SM->SetCollisionResponseToAllChannels(ECR_Ignore);
	Food_SM->SetupAttachment(RootComponent);

	// Game points
	Score = 50;

	// Default value Interval
	UpdateSecond = 0.01f;

	LastFoodType = EFoodType::FirstType;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();	

	// Get current game mode
	GM = Cast<ASnakeGameMode>(GetWorld()->GetAuthGameMode());
	if (GM)
	{
		// Set size box collision 
		FVector BoxSize = FVector((GM->GetElementSize() * GM->GetElementScale()) / 2);
		BoxCollision->SetBoxExtent(BoxSize);
	}

	Food_SM->SetMaterial(0, MatFirstType);
	ChangeFoodType();
}

// Interaction of food with snakes
void AFood::Interact(AActor* Interacted)
{
	auto Snake = Cast<ASnake>(Interacted);
	if (IsValid(Snake))
	{
		Snake->AddNewSnakeElement();

		// Increase game points
		if (IsValid(GM)) GM->AddScore(Score);

		// Play the sound of eating food
		if (EatingFoodSound) UGameplayStatics::PlaySoundAtLocation(this, EatingFoodSound, GetActorLocation());

		OnSpawnFood.Broadcast();
		Destroy();
	}
}

// Start function UpdateScore through interval
void AFood::ChangeFoodType()
{
	GetWorld()->GetTimerManager().SetTimer(UpdateScoreTimer, this, &AFood::UpdateScore, UpdateSecond, false);	
}

// Set the current number of points
void AFood::UpdateScore()
{
	GetWorld()->GetTimerManager().ClearTimer(UpdateScoreTimer);

	switch (LastFoodType)
	{
	case EFoodType::FirstType:
		Score = 50;
		UpdateSecond = 3.f;
		Food_SM->SetMaterial(0, MatFirstType);
		LastFoodType = EFoodType::SecondType;
		break;
	case EFoodType::SecondType:
		Score = 25;
		UpdateSecond = 4.f;
		Food_SM->SetMaterial(0, MatSecondType);
		LastFoodType = EFoodType::ThirdTipe;
		break;
	case EFoodType::ThirdTipe:
		Score = 15;
		UpdateSecond = 5.f;
		Food_SM->SetMaterial(0, MatThirdType);
		LastFoodType = EFoodType::FourthType;
		break;
	case EFoodType::FourthType:
		Score = 5;
		Food_SM->SetMaterial(0, MatFourthType);
		break;
	}

	ChangeFoodType();
}