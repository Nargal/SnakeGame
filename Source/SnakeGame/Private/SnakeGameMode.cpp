#include "SnakeGameMode.h"

// My class include
#include "Camera/SnakeCamera.h"
#include "Levels/GridGenerator.h"
#include "Snake/Snake.h"
#include "Food/Food.h"
#include "UI/SnakeMainUI.h"

ASnakeGameMode::ASnakeGameMode()
{
	DefaultPawnClass = ASnakeCamera::StaticClass();

	// Default snake step delay
	StepDelay = .45f;

	// Number of elements on the X-axis
	GridSizeX = 20;
	// Number of elements on the Y-axis
	GridSizeY = 20;

	// Size of a single element
	ElementSize = 100.f;
	// Scale of a single element
	ElementScale = 1.0f;
	// The padding of the element
	ElementPadding = 1.f;

	bGame = false;
}

void ASnakeGameMode::BeginPlay()
{
	DefaultPC = GetWorld()->GetFirstPlayerController();
	if (IsValid(DefaultPC))
	{
		// Setup an input mode that allows only player input / player controller to respond to user input.  
		DefaultPC->SetInputMode(FInputModeGameOnly());

		// Get current pawn 
		SnakePawn = Cast<ASnakeCamera>(DefaultPC->GetPawn());
	}

	// Create main HUD
	CreateSnakeUI();

	// Start new game
	StartGame();	
}

void ASnakeGameMode::StartGame()
{
	bGame = true;

	if (IsValid(SnakeUIRef))
	{
		// Show snake UI
		SnakeUIRef->AddToViewport();
	}

	// Creating a game map
	GeneratingMap();

	// Create and spawn new snake
	CreateSnake();

	// Create and spawn new food
	SpawnNewFood();
}

// Create main HUD
void ASnakeGameMode::CreateSnakeUI()
{
	if (SnakeClassUI != nullptr)
	{
		SnakeUIRef = CreateWidget<USnakeMainUI>(DefaultPC, SnakeClassUI);
	}		
}

// Create and spawn new snake
void ASnakeGameMode::GeneratingMap()
{
	if (GridActorClass)
	{
		GridActorRef = GetWorld()->SpawnActor<AGridGenerator>(GridActorClass, FTransform());
		if (IsValid(GridActorRef))
		{
			GridActorRef->GridGeneration(GridSizeX, GridSizeY, ElementSize, ElementScale, ElementPadding);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ASnakeGameMode::GeneratingMap(): GridActorClass is nullptr. Please update SnakeGameMode class with valid subclass."));
	}
}

// Create and spawn new snake
void ASnakeGameMode::CreateSnake()
{
	if (SnakeClass)
	{
		SnakeRef = GetWorld()->SpawnActor<ASnake>(SnakeClass, FTransform());
		if (IsValid(SnakeRef))
		{
			SnakeRef->OnSnakeDeath.AddDynamic(this, &ASnakeGameMode::SetGameOverUI);
			if (IsValid(SnakePawn))
			{
				SnakePawn->SetSnakeActor(SnakeRef);
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ASnakeGameMode::CreateSnake(): SnakeClass is nullptr. Please update SnakeGameMode class with valid subclass."));
	}
}

// Create and spawn new food
void ASnakeGameMode::SpawnNewFood()
{
	if (IsValid(FoodClass) && IsValid(GridActorRef))
	{
		FoodActor = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(GridActorRef->GetRandomLocation()));
		if (FoodActor)
		{
			FoodActor->OnSpawnFood.AddDynamic(this, &ASnakeGameMode::SpawnNewFood);
			if (IsValid(SnakeRef))
			{
				SnakeRef->ChangeDelayMoving(0.01f);
			}
		}
	}
}

// Create main HUD
void ASnakeGameMode::SetGameOverUI()
{
	SnakeUIRef->GameOver();

	bGame = false;
}

// Restarting the game
void ASnakeGameMode::RestartingGame()
{
	// Removes the widget.
	if (IsValid(SnakeUIRef)) SnakeUIRef->RemoveFromParent();

	// Remove the snake and all its elements
	if (IsValid(SnakeRef)) SnakeRef->DestroySnake();

	// Remove spawn food
	if (IsValid(FoodActor)) FoodActor->Destroy();

	// Remove spawn food
	if (IsValid(GridActorRef)) GridActorRef->DestroyGrid();

	// Reset Score
	Score = 0;

	StartGame();
}


// Increase score by Value 
void ASnakeGameMode::AddScore(int32 Value)
{
	Score += Value;
}

// Return current score
int32 ASnakeGameMode::GetScore() { return Score; }
// Return Snake movement step delay
float ASnakeGameMode::GetStepDelay() {	return StepDelay; }
// Returns the size of a single element
float ASnakeGameMode::GetElementSize() { return ElementSize; }
// Returns the scale of a single element
float ASnakeGameMode::GetElementScale() { return ElementScale; }
// Returns the padding from the element
float ASnakeGameMode::GetElementPadding() { return ElementPadding; }
// Return game status
bool ASnakeGameMode::GetGameStatus() { return bGame; }