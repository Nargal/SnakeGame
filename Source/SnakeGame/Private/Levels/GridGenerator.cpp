#include "Levels/GridGenerator.h"

// UE4 class include 
#include "Components/SceneComponent.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"

// My class include
#include "Levels/GridElementCollision.h"

// Sets default values
AGridGenerator::AGridGenerator()
{
	SceneRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRootComponent"));
	SceneRootComponent = RootComponent;

	GridElement_HISM = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("GridElement_HISM"));
	GridElement_HISM->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GridElement_HISM->SetupAttachment(RootComponent);
}

void AGridGenerator::GridGeneration(int32 GridSizeX, int32 GridSizeY, float ElementSize, float ElementScale, float ElementPadding)
{
	float StepGrid = (ElementSize + ElementPadding) * ElementScale;

	float GridWidth = GridSizeX * StepGrid;
	float GridHeight = GridSizeY * StepGrid;

	float StartSpawnX = GridWidth / 2 * -1;
	float StartSpawnY = GridHeight / 2 * -1;
	float StartSpawnZ = 0;

	FVector GridOffset = FVector(StartSpawnX, StartSpawnX, StartSpawnZ);

	for (int i = 0; i < GridSizeX; i++)
	{
		for (int j = 0; j < GridSizeY; j++)
		{
			FRotator SpawnRotation(0.f, 0.f, 0.f);
			FVector SpawnLocation = FVector(StepGrid * i, StepGrid * j, -StepGrid) + GridOffset;
			FVector SpawnScale(ElementScale);
			FTransform SpawnTransform(SpawnRotation, SpawnLocation, SpawnScale);

			GridElement_HISM->AddInstance(SpawnTransform);
			CreateCollision(ElementSize, ElementScale, SpawnLocation);

			SpawnLocation.Z = 0;
			SpawnTransform = FTransform(SpawnRotation, SpawnLocation, SpawnScale);

			if (i == 0 || i == GridSizeX - 1 || j == 0 || j == GridSizeY - 1)
			{				
				GridElement_HISM->AddInstance(SpawnTransform);
				CreateCollision(ElementSize, ElementScale, SpawnLocation);
			}
			else
			{
				SpawnElemsLocation.Add(SpawnLocation);
			}
		}
	}
}

void AGridGenerator::CreateCollision(float ElementSize, float ElementScale, FVector SpawnLocation)
{
	if (ElementCollisionClass)
	{
		AGridElementCollision* NewCollision = GetWorld()->SpawnActor<AGridElementCollision>(ElementCollisionClass, FTransform(SpawnLocation));
		if (IsValid(NewCollision))
		{
			NewCollision->SetBoxSize(ElementSize, ElementScale);
			ElementsCollision.Add(NewCollision);
		}
	}
}

FVector AGridGenerator::GetRandomLocation()
{
	int32 RandomIndex = FMath::RandRange(0, SpawnElemsLocation.Num() - 1);
	return SpawnElemsLocation[RandomIndex];
}

// Destroy all grid element
void AGridGenerator::DestroyGrid()
{
	SpawnElemsLocation.Empty();

	for (int i = 0; i < ElementsCollision.Num(); i++)
	{
		ElementsCollision[i]->Destroy();
	}

	ElementsCollision.Empty();
	Destroy();
}