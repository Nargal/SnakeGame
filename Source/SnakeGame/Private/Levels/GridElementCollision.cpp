#include "Levels/GridElementCollision.h"

// UE4 class include
#include "Components/BoxComponent.h"

// My class include
#include "Snake/Snake.h"

// Sets default values
AGridElementCollision::AGridElementCollision()
{
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetBoxExtent(FVector(50.f));
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	BoxCollision->SetCollisionObjectType(ECC_WorldStatic);
	BoxCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxCollision->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Block);
	BoxCollision->SetupAttachment(RootComponent);
}

// Set new box size
void AGridElementCollision::SetBoxSize(float Size, float Scale)
{
	FVector BoxSize = FVector((Size * Scale) / 2);
	BoxCollision->SetBoxExtent(BoxSize);
}

void AGridElementCollision::Interact(AActor* Interacted)
{
	UE_LOG(LogTemp, Warning, TEXT("TEST"));

	auto Snake = Cast<ASnake>(Interacted);
	if (IsValid(Snake))
	{
		Snake->StartSnakeDead();		
	}
}