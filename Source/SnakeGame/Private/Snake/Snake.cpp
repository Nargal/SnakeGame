#include "Snake/Snake.h"

// UE4 class include
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

// My class include
#include "SnakeGameMode.h"
#include "Snake/SnakeElement.h"
#include "Interface/Interactable.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetBoxExtent(FVector(50.f));
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxCollision->SetCollisionObjectType(ECC_PhysicsBody);
	BoxCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxCollision->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);
	BoxCollision->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	RootComponent = BoxCollision;

	Head_SM = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Head_SM"));
	Head_SM->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Head_SM->SetCollisionResponseToAllChannels(ECR_Ignore);
	Head_SM->SetupAttachment(RootComponent);

	// Clear buffer
	BufferTime = 0.f;

	// Default snake step delay
	StepDelay = 0.5f;

	// Size step grid
	StepSize = 0.f;
	// Size of a single element
	ElementSize = 0.f;
	// Scale of a single element
	ElementScale = 1.0f;
	// The padding of the element
	ElementPadding = 0.f;
	
	// Default direction
	MoveDirection = FVector(-1.f, 0.f, 0.f);

	// Current direction
	CurrentMoveDir = FVector(ForceInitToZero);

	// Element location
	PrevElementLocation = FVector(ForceInitToZero);

	bIsAlive = true;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();	

	auto GM = Cast<ASnakeGameMode>(GetWorld()->GetAuthGameMode());
	if (GM)
	{
		StepDelay = GM->GetStepDelay();
		
		ElementSize = GM->GetElementSize();
		ElementScale = GM->GetElementScale();
		ElementPadding = GM->GetElementPadding();

		StepSize = (ElementSize * ElementScale) + ElementPadding;

		Head_SM->SetWorldScale3D(FVector(ElementScale) + 0.05f);
	}

	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &ASnake::SnakeElementOverlap);

	AddNewSnakeElement(3);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BufferTime += DeltaTime;
	if (BufferTime > StepDelay && bIsAlive)
	{
		MoveSnake();
		BufferTime = .0f;
	}
}

void ASnake::AddNewSnakeElement(int32 NumElement)
{
	if (SnakeElementClass == nullptr) return;

	for (int i = 0; i < NumElement; i++)
	{
		FRotator SpawnRotation(0.f, 0.f, 0.f);
		FVector SpawnLocation(GetActorLocation() - (MoveDirection * StepSize));
		FVector SpawnScale(ElementScale);
		FTransform SpawnTranform(SpawnRotation, SpawnLocation, SpawnScale);

		ASnakeElement* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, SpawnTranform);
		SnakeElements.Add(NewSnakeElement);

		if (i > 0)
		{
			MoveSnake();
		}		
	}
}

// Set new snake movement direction
void ASnake::SetMovementDirection(FVector NewDirection)
{
	MoveDirection = NewDirection;
}

// Return current direction of movement of the snake's head
FVector ASnake::GetCurrentMoveDir()
{
	return CurrentMoveDir;
}

void ASnake::SnakeElementOverlap(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Interface = Cast<IInteractable>(OtherActor);
	if (Interface)
	{
		Interface->Interact(this);
	}
}

void ASnake::StartSnakeDead()
{
	if (bIsAlive)
	{
		bIsAlive = false;

		if (SnakeDestroySound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, SnakeDestroySound, GetActorLocation());
		}

		FTimerHandle Handle;
		GetWorld()->GetTimerManager().SetTimer(Handle, this, &ASnake::SnakeDeath, 3.0f, false);
	}	
}

void ASnake::SnakeDeath()
{
	if (HeadDead_FX)
	{
		PS_HeadDead = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HeadDead_FX, GetActorLocation());
	}

	FVector InpulseRotation(ForceInitToZero);

	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		InpulseRotation.X = FMath::RandRange(-360, 360);
		InpulseRotation.Y = FMath::RandRange(-360, 360);
		InpulseRotation.Z = 1000;

		SnakeElements[i]->SnakeElementDead(InpulseRotation, 1.f);
	}

	OnSnakeDeath.Broadcast();
}

void ASnake::ChangeDelayMoving(float Value)
{
	StepDelay -= Value;
	if (StepDelay < 0.1f)
	{
		StepDelay = 0.1f;
	}
}

// The snake moves once in the time interval
void ASnake::MoveSnake()
{
	PrevElementLocation = GetActorLocation();

	AddActorWorldOffset(MoveDirection * StepSize);

	CurrentMoveDir = GetActorLocation() - PrevElementLocation;

	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		FVector CurrentElementLocation = SnakeElements[i]->GetActorLocation();
		SnakeElements[i]->SetActorLocation(PrevElementLocation);
		PrevElementLocation = CurrentElementLocation;
	}
}

void ASnake::DestroySnake()
{
	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		SnakeElements[i]->Destroy();
	}

	if (IsValid(PS_HeadDead)) PS_HeadDead->DestroyComponent();
	
	Destroy();
}