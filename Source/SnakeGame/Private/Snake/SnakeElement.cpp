#include "Snake/SnakeElement.h"

// UE4 class include
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"

// My class include
#include "SnakeGameMode.h"
#include "Snake/Snake.h"

// Sets default values
ASnakeElement::ASnakeElement()
{
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetBoxExtent(FVector(50.f));
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxCollision->SetCollisionObjectType(ECC_PhysicsBody);
	BoxCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxCollision->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);
	BoxCollision->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	RootComponent = BoxCollision;

	SnakeElement_SM = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SnakePart_SM"));
	SnakeElement_SM->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SnakeElement_SM->SetCollisionResponseToAllChannels(ECR_Ignore);
	SnakeElement_SM->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASnakeElement::BeginPlay()
{
	Super::BeginPlay();	

	auto GM = Cast<ASnakeGameMode>(GetWorld()->GetAuthGameMode());
	if (GM)
	{
		FVector BoxSize = FVector((GM->GetElementSize() * GM->GetElementScale()) / 2);
		BoxCollision->SetBoxExtent(BoxSize);
	}
}

void ASnakeElement::Interact(AActor* Interacted)
{
	auto Snake = Cast<ASnake>(Interacted);
	if (IsValid(Snake))
	{
		Snake->StartSnakeDead();
	}
}

void ASnakeElement::SnakeElementDead(FVector IpulseVector, float MassElement)
{
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	BoxCollision->SetSimulatePhysics(true);
	BoxCollision->SetMassOverrideInKg(FName(), MassElement);
	BoxCollision->AddImpulse(FVector(IpulseVector));
}